﻿// perco_ir05_reader.cpp : Определяет точку входа для приложения.
//

#include "stdafx.h"
#include "perco_ir05_reader.h"
#include "CPercoReader.h"
#define PERCO_FORM_READ_CARD 1001
#define PERCO_FORM_STOP_READ_CARD 1002


#define MAX_LOADSTRING 100

// Глобальные переменные:
HINSTANCE hInst;                                // текущий экземпляр
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

bool STOP_READ_FLAG = false;
HANDLE read_thread = nullptr;
void DetectComPorts(vector< wstring >& ports, size_t upperLimit = 16)
{
	for (size_t i = 1; i <= upperLimit; i++)
	{
		TCHAR strPort[32] = { 0 };
		_stprintf_s(strPort, 32, _T("COM%d"), i);

		DWORD dwSize = 0;
		LPCOMMCONFIG lpCC = (LPCOMMCONFIG) new BYTE[1];
		BOOL ret = GetDefaultCommConfig(strPort, lpCC, &dwSize);
		delete[] lpCC;

		lpCC = (LPCOMMCONFIG) new BYTE[dwSize];
		ret = GetDefaultCommConfig(strPort, lpCC, &dwSize);
		delete[] lpCC;

		if (ret) ports.push_back(strPort);
	}
}
// Отправить объявления функций, включенных в этот модуль кода:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI GetCardThread();
HWND CreateButton(HWND & parrent, LPTSTR text, int x = 0, int y = 0, int width = 100, int height = 20, int menu = 10000, bool disabled = false);
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Разместите код здесь.

	// Инициализация глобальных строк
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_PERCOIR05READER, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Выполнить инициализацию приложения:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PERCOIR05READER));

	MSG msg;

	// Цикл основного сообщения:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  ЦЕЛЬ: Регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PERCOIR05READER));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = L"";
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));



	return RegisterClassExW(&wcex);
}

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   ЦЕЛЬ: Сохраняет маркер экземпляра и создает главное окно
//
//   КОММЕНТАРИИ:
//
//        В этой функции маркер экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится главное окно программы.
//
HWND BTN_READ_CARD;
HWND BTN_STOP_READ;
HWND EDIT_CARD_SERIES;
HWND EDIT_CARD_PART_NUMBER;
HWND EDIT_CARD_NUMBER;
HWND CBOX_COM;
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Сохранить маркер экземпляра в глобальной переменной

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU ,
		CW_USEDEFAULT, CW_USEDEFAULT, 200, 200, nullptr, nullptr, hInstance, nullptr);
	//Создание кнопки считать
	BTN_READ_CARD = CreateButton(hWnd, (LPTSTR)L"Считать", 15, 125, 70, 30, PERCO_FORM_READ_CARD);
	//Создание кнопки остановить
	BTN_STOP_READ = CreateButton(hWnd, (LPTSTR)L"Стоп", 95, 125, 70, 30, PERCO_FORM_STOP_READ_CARD, true);
	HWND LBL_CARD_NUMBER = CreateWindowW(L"static", L"Номер",
		WS_CHILD | WS_VISIBLE,
		10, 77, 160, 20, hWnd, nullptr, nullptr, nullptr);
	EDIT_CARD_NUMBER = CreateWindowW(L"Edit", L"",
		WS_BORDER | WS_CHILD | WS_VISIBLE,
		10, 95, 160, 20, hWnd, nullptr, nullptr, nullptr);
	HWND LBL_CARD_SERIES_AND_NUMBER = CreateWindowW(L"static", L"Серия и номер",
		WS_CHILD | WS_VISIBLE,
		10, 38, 160, 20, hWnd, nullptr, nullptr, nullptr);
	EDIT_CARD_SERIES = CreateWindowW(L"Edit", L"",
		WS_BORDER | WS_CHILD | WS_VISIBLE,
		10, 55, 40, 20, hWnd, nullptr, nullptr, nullptr);
	EDIT_CARD_PART_NUMBER = CreateWindowW(L"Edit", L"",
		WS_BORDER | WS_CHILD | WS_VISIBLE,
		70, 55, 100, 20, hWnd, nullptr, nullptr, nullptr);
	vector< wstring > ports;
	DetectComPorts(ports);
	CBOX_COM = CreateWindowW(L"comboBox", NULL, WS_CHILD | WS_VISIBLE | LBS_STANDARD,
		10, 10, 160, 100, hWnd, NULL, hInst, NULL);
	// Отменяем режим перерисовки списка
	SendMessage(CBOX_COM, WM_SETREDRAW, FALSE, 0L);
	// Добавляем в список несколько строк
	for (auto port : ports) {
		SendMessage(CBOX_COM, CB_ADDSTRING, 0,
			(LPARAM)(LPSTR)port.c_str());
	}
	SendMessage(CBOX_COM, WM_SETREDRAW, TRUE, 0L);

	// Перерисовываем список
	InvalidateRect(CBOX_COM, NULL, TRUE);
	if (!hWnd)
	{
		return FALSE;
	}


	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}



//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//  ЦЕЛЬ: Обрабатывает сообщения в главном окне.
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Разобрать выбор в меню:
		switch (wmId)
		{
		case PERCO_FORM_READ_CARD:
			EnableWindow(BTN_READ_CARD, false);
			EnableWindow(BTN_STOP_READ, true);
			STOP_READ_FLAG = false;
			read_thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)&GetCardThread, NULL, 0, NULL);
			SetWindowText(EDIT_CARD_NUMBER, L"Считываем");
			break;
		case PERCO_FORM_STOP_READ_CARD:
			STOP_READ_FLAG = true;
			if (WaitForSingleObject(read_thread, 10000) == WAIT_TIMEOUT) {
				TerminateThread(read_thread, 0);
				EnableWindow(BTN_READ_CARD, true);
				EnableWindow(BTN_STOP_READ, false);
			}
			read_thread = nullptr;
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

HWND CreateButton(HWND & parrent, LPTSTR text, int x, int y, int width, int height, int menu, bool disabled) {
	UINT flags = WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON;
	if (disabled)
		flags |= WS_DISABLED;
	return CreateWindowW(
		L"BUTTON",  // Predefined class; Unicode assumed 
		text,      // Button text 
		flags,  // Styles 
		x,         // x position 
		y,         // y position 
		width,        // Button width
		height,        // Button height
		parrent,     // Parent window
		(HMENU)menu,       // No menu.
		hInst,
		nullptr);      // Pointer not needed.
}\
DWORD WINAPI GetCardThread() {
	wchar_t text[64];
	GetWindowTextW(CBOX_COM, text, 64);
	wstring com_port = text;
	if (com_port.empty()) {
		SetWindowText(EDIT_CARD_NUMBER, L"Выбирите COM порт");
		EnableWindow(BTN_READ_CARD, true);
		EnableWindow(BTN_STOP_READ, false);
		return 0;
	}
	CPercoReader reader(com_port.c_str());
	vector<byte> ret = reader.Start(STOP_READ_FLAG);
	unsigned int val = 0;
	for (auto b : ret) {
		val = val << 8;
		val += b;
	}
	if (ret.size() == 5) {
		SetWindowText(EDIT_CARD_SERIES, to_wstring(ret[2]).c_str());
		SetWindowText(EDIT_CARD_PART_NUMBER, to_wstring(ret[3]*256+ret[4]).c_str());
	}
	SetWindowText(EDIT_CARD_NUMBER, to_wstring(val).c_str());
	EnableWindow(BTN_READ_CARD, true);
	EnableWindow(BTN_STOP_READ, false);
	return 0;
}