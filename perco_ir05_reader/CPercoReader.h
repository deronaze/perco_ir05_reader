#pragma once
class CPercoReader
{
private:
	HANDLE hPort;
	LPCTSTR sPortName;
	void StartupComPort();
	void WriteComPort();
	vector<byte> ReadComPort();
public:
	CPercoReader(LPCTSTR sPortName);
	~CPercoReader();

	vector<byte> Start(bool & stop_flag);
	
};

