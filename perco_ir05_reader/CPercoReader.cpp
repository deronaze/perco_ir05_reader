#include "stdafx.h"
#include "CPercoReader.h"

#include <tchar.h>
#include <stdio.h>
#include <windows.h>
#include <iostream>

using namespace std;


DCB dcbSerialParam = { 0 };

CPercoReader::CPercoReader(LPCTSTR sPortName) : sPortName(sPortName)
{
}
void CPercoReader::StartupComPort(void) {
	byte rand_byte[] = { 70 };
	byte b_start_sequence[] = { 242, 255, 3, 1, 0, 64, 66 };
	DWORD size_start_sequence = sizeof(b_start_sequence);
	DWORD dwBytesWritten;
	hPort = CreateFileW(sPortName, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

	if (hPort == INVALID_HANDLE_VALUE)
	{
		if (GetLastError() == ERROR_FILE_NOT_FOUND)
		{
			exit(-1);
		}
		else
		{
			exit(-1);
		}
	}
	dcbSerialParam.DCBlength = sizeof(dcbSerialParam);
	if (!GetCommState(hPort, &dcbSerialParam))
	{
		exit(-1);
	}
	dcbSerialParam.BaudRate = CBR_9600;
	dcbSerialParam.ByteSize = 8;
	dcbSerialParam.StopBits = ONESTOPBIT;
	dcbSerialParam.Parity = NOPARITY;

	if (!SetCommState(hPort, &dcbSerialParam))
	{
		exit(-1);
	}
	// set read timeout
	COMMTIMEOUTS timeouts;
	timeouts.ReadIntervalTimeout = 0;
	timeouts.ReadTotalTimeoutMultiplier = 0;
	timeouts.ReadTotalTimeoutConstant = 100;
	timeouts.WriteTotalTimeoutMultiplier = 0;
	timeouts.WriteTotalTimeoutConstant = 0;
	SetCommTimeouts(hPort, &timeouts);
	EscapeCommFunction(hPort, SETRTS);
	WriteFile(hPort, rand_byte, sizeof(rand_byte), &dwBytesWritten, NULL);
	EscapeCommFunction(hPort, SETRTS);
	EscapeCommFunction(hPort, CLRRTS);
	PurgeComm(hPort, PURGE_TXCLEAR | PURGE_RXCLEAR);
	WriteFile(hPort, b_start_sequence, size_start_sequence, &dwBytesWritten, NULL);
	this->ReadComPort();
}


vector<byte> CPercoReader::Start(bool & stop_flag) {
	this->StartupComPort();
	while (!stop_flag)
	{
		PurgeComm(this->hPort, PURGE_TXCLEAR | PURGE_RXCLEAR);
		this->WriteComPort();
		vector<byte> ret = this->ReadComPort();
		if (!ret.empty()) {
			CloseHandle(this->hPort);
			return ret;
		}
	}
	CloseHandle(this->hPort);
	return vector<byte>();
}

void CPercoReader::WriteComPort(void) {
	byte b_read_sequence[] = { 242, 255, 1, 2, 3 };
	DWORD size_sequence = sizeof(b_read_sequence);
	DWORD dwBytesWritten = 0;
	WriteFile(this->hPort, b_read_sequence, size_sequence, &dwBytesWritten, NULL);
}
vector<byte> CPercoReader::ReadComPort(void)
{
	DWORD iSize;
	byte buffer[64];
	int len = sizeof(buffer);
	memset(buffer, 0, len);
	OutputDebugStringW(to_wstring(GetLastError()).c_str());
	ReadFile(this->hPort, &buffer, len, &iSize, 0);

	if (iSize > 0)
	{
		if (buffer[4] == 128) {
			vector<byte> b_identifier(5);
			b_identifier[2] = buffer[9]; //series
			b_identifier[3] = buffer[8]; // first half number
			b_identifier[4] = buffer[7]; //second half number
			return b_identifier;
		}
		/*other solution from not work for PERCo-IR05 (https://github.com/yadogma/PERCo-IR05.2)
		for (int i = 2; i < sizeof(buffer); i = i + 8)
		{
			if (buffer[i] == 8)
			{
				vector<byte> b_identifier(5);
				//����� ���� ������������������ �� 1 ����
				b_identifier[2] = (buffer[i + 7] >> 1) + (buffer[i + 8] & 1) * 128; //series
				b_identifier[3] = (buffer[i + 6] >> 1) + (buffer[i + 7] & 1) * 128; // first half number
				b_identifier[4] = (buffer[i + 5] >> 1) + (buffer[i + 6] & 1) * 128; //second half number
				return b_identifier;
			}
		}*/
	}
	return vector<byte>();
}



CPercoReader::~CPercoReader()
{
}
